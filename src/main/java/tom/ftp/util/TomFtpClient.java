package tom.ftp.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class TomFtpClient {

    private Logger logger = LoggerFactory.getLogger(TomFtpClient.class);
    private FtpConnInfo connInfo;

    public static TomFtpClient connect(String ftpAddr) throws MalformedURLException {
        return new TomFtpClient(ftpAddr);
    }

    public void downloadFile(String fileWithPath, FtpFileHandler ftpFileHandler) throws IOException {
        logger.info("downland file, connect to : {}", this.connInfo);
        FTPClient client = new FTPClient();
        client.connect(connInfo.getHost(), connInfo.getPort());
        client.login(connInfo.getUser(), connInfo.getPass());
        try {
            writeToFile(client, connInfo.getFtpAddr(), FilenameUtils.getFullPath(fileWithPath), FilenameUtils.getName(fileWithPath), ftpFileHandler);
        } finally {
            client.disconnect();
        }
    }

    public void download(String rootPath, FtpFileHandler ftpFileHandler) throws IOException {
        logger.info("downland, connect to : {}", this.connInfo);
        FTPClient client = new FTPClient();
        client.connect(connInfo.getHost(), connInfo.getPort());
        client.login(connInfo.getUser(), connInfo.getPass());
        try {
            for (FTPFile ftpFile : client.listFiles(rootPath)) {
                if (ftpFile.isFile()) {
                    logger.info("write File to output stream {}", ftpFile);
                    writeToFile(client, connInfo.getFtpAddr(), rootPath, ftpFile.getName(), ftpFileHandler);
                }
            }
            ;
        } finally {
            client.disconnect();
        }
    }

    public void downloadAll(String rootPath, FtpFileHandler ftpFileHandler) throws IOException {
        logger.info("download all, connect to : {}", this.connInfo);
        FTPClient client = new FTPClient();
        client.connect(connInfo.getHost(), connInfo.getPort());
        client.login(connInfo.getUser(), connInfo.getPass());
        try {
            downloadDirectory(client, connInfo.getFtpAddr(), rootPath, ftpFileHandler);
        } finally {
            client.disconnect();
        }
    }

    public TomFtpClient withAuth(String user, String password) {
        this.connInfo.setUser(user);
        this.connInfo.setPass(password);
        return this;
    }

    private TomFtpClient(String ftpAddr) {
        try {
            this.connInfo = parseFtpAddr(ftpAddr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private FtpConnInfo parseFtpAddr(String ftpAddr) throws MalformedURLException {
        URL u = new URL(ftpAddr);
        String user = "anonymous";
        String pass = "a@b.cn";
        String userInfo = u.getUserInfo();
        if (!StringUtils.isEmpty(userInfo)) {
            user = userInfo.split(":")[0];
            pass = userInfo.split(":")[1];
        }
        return new FtpConnInfo(ftpAddr, user, pass, u.getHost(), u.getPort(), u.getPath());
    }

    private void downloadDirectory(FTPClient client, String ftpRoot, String path, FtpFileHandler ftpFileHandler) throws IOException {
        for (FTPFile ftpFile : client.listFiles(path)) {
            if (ftpFile.isDirectory()) {
                downloadDirectory(client, ftpRoot, touch(path, ftpFile.getName()), ftpFileHandler);
            } else {
                logger.info("write File to output stream {}", ftpFile);
                writeToFile(client, ftpRoot, path, ftpFile.getName(), ftpFileHandler);
            }
        }
        ;
    }

    public static String touch(String path, String name) {
        return removeLastSlash(path) + "/" + removeFirstSlash(name);
    }

    private static String removeFirstSlash(String name) {
        if(name.startsWith("/")){
            return name.substring(1);
        }
        return name;
    }

    private static String removeLastSlash(String name) {
        if(name.endsWith("/")){
            return name.substring(0, name.length()-1);
        }
        return name;
    }

    private void writeToFile(FTPClient client, String ftpRoot, String path, String name, FtpFileHandler ftpFileHandler) throws IOException {
        ftpFileHandler.store(new TomFileImpl(client, ftpRoot, path, name));
    }


}
