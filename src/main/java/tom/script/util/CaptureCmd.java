package tom.script.util;

import org.apache.commons.exec.CommandLine;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class CaptureCmd extends BashCmd {

    public CaptureCmd(String cmdScritpRoot) {
        super(cmdScritpRoot);
    }

    public void capture(File f, String timeSlice, String path) {
        logger.info("capture file {}, {}", f,path);
        CommandLine cmdLine = makeCmd("/capture.sh");
        Map map = new HashMap();
        map.put("file", f);
        cmdLine.addArgument("${file}");
        cmdLine.addArgument(timeSlice);
        cmdLine.addArgument(path);
        cmdLine.setSubstitutionMap(map);
        try {
            executeCmd(cmdLine, 1);
        } catch (Exception e) {
            logger.error("execute cmd error.", e);
        }
    }

    public void captureLive(File f, String streamUrl) {
        logger.info("capture live {}, {}", f,streamUrl);
        CommandLine cmdLine = makeCmd("/captureLive.sh");
        Map map = new HashMap();
        map.put("file", f);
        cmdLine.addArgument("${file}");
        cmdLine.addArgument(streamUrl);
        cmdLine.setSubstitutionMap(map);
        try {
            executeCmd(cmdLine, 1);
        } catch (Exception e) {
            logger.error("execute cmd error.", e);
        }
    }


}
