#!/bin/sh -e
ORIGIN_FILE=$1
HDFS_PATH=$2
FILENAME=$3
#HDFS_ROOT=/Users/xuhaixiang/var/red/hdfsroot
HDFS_ROOT=/home/genvision/workspace/data/hdfsroot
echo "upload to hdfs"
echo "usage: three parameter: originfile, hdfspath, filename"
echo "copy file from : $ORIGIN_FILE to hdfs with path $HDFS_PATH and file name $FILENAME"
echo "mkdir -p $HDFS_ROOT$HDFS_PATH"
echo "cp $ORIGIN_FILE $HDFS_ROOT$HDFS_PATH"/"$FILENAME"
mkdir -p $HDFS_ROOT$HDFS_PATH
cp $ORIGIN_FILE $HDFS_ROOT$HDFS_PATH"/"$FILENAME