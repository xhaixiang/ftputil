#!/bin/sh -e
LIVE_ADDR=$1
LOCAL_FILE=$2
TIME_SLICE=$3
echo "record remote live stream to local file. $LIVE_ADDR, $LOCAL_FILE, $TIME_SLICE is Seconds"
echo "/home/genvision/apps/ffmpeg/ffmpeg -t $TIME_SLICE -i $LIVE_ADDR -c copy $LOCAL_FILE"
/home/genvision/apps/ffmpeg/ffmpeg -t $TIME_SLICE -i $LIVE_ADDR -c copy $LOCAL_FILE